package org.ideoholic.communication.dto;

public class GroupsDTO {
	
	private String groupName;
	
	private String id;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
