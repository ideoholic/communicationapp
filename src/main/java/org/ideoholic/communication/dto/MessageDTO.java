package org.ideoholic.communication.dto;

import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;


public class MessageDTO {

	
	@Id
	private String id;
	
	@NotBlank
	@Field(value="subject")
	private String subject;
	
	@Field(value="description")
	private String description;
	
	@NotBlank
	@Field(value="group") 
	private String group;
	
	
	@Field(value="time")
	private String time;
	

	@Field(value="datepicker")
	private String datepicker;
	
	@Field(value="timepicker")
	private String timepicker;
	
	
	public String getDatepicker() {
		return datepicker;
	}

	public void setDatepicker(String datepicker) {
		this.datepicker = datepicker;
	}
	
	public String getTimepicker() {
		return timepicker;
	}

	public void setTimepicker(String timepicker) {
		this.timepicker = timepicker;
	}

	@Field(value="date")
	private String date;
	
	@Field(value="userid")
	private String userId;
	
	@Field(value="users")
	private List<String> users;
	
	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroup() { return group; }
	 
	public void setGroup(String group) { this.group = group; }

	public void setCreatedBy(Object createdBy) {
		// TODO Auto-generated method stub
		
	}
	
	
}
