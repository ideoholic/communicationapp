package org.ideoholic.communication.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SendSms {
	
	// static variable single_instance of type Singleton 
    private static SendSms single_instance = null; 
    
    private SendSms() {
    	
    }
    

    // static method to create instance of Singleton class 
    public static SendSms getInstance() 
    { 
        if (single_instance == null) 
            single_instance = new SendSms(); 
  
        return single_instance; 
    } 
    
    //code to send SMS
  	public int sendSmsFunc(String numbers, String message) {
  	int responseCode = 0;
  	try 
  		{     
  		 String smsuser = "CURIUM";
  		 String smssender = "CURIUM";
  		 String apikey = "1-1-1-1";
  		          
  		 // Construct data
  		 String phonenumbers=numbers;
  		 String data="username=" + URLEncoder.encode(smsuser, "UTF-8");
  		 data +="&message=" + URLEncoder.encode(message, "UTF-8");
  		 data +="&sendername=" + URLEncoder.encode(smssender, "UTF-8");
  		 data +="&smstype=" + "PROMO";
  		 data +="&numbers=" + URLEncoder.encode(phonenumbers, "UTF-8");
  		 data +="&apikey=" + apikey;
  		          
  		 // Send data
  		 String POST_URL = "http://sms.bulksmsind.in/sendSMS?"+data;
  		 URL obj = new URL(POST_URL);
  		 HttpURLConnection con = (HttpURLConnection) obj.openConnection();
  		 con.setRequestMethod("POST");

  		 // For POST only - START
  		 con.setDoOutput(true);
  		 OutputStream os = con.getOutputStream();
  		 
  		 //os.write("CURIUM".getBytes());
  		 os.flush();
  		 os.close();
  		 // For POST only - END
  		      
  		 responseCode = con.getResponseCode();
  		 //     LOGGER.info("POST Response Code :: " + responseCode);

  		 if (responseCode == HttpURLConnection.HTTP_OK) { //success
  		 BufferedReader in = new BufferedReader(new InputStreamReader(
  		 con.getInputStream()));
  		 String inputLine;
  		 StringBuffer response = new StringBuffer();

  		 while ((inputLine = in.readLine()) != null) {
  		 response.append(inputLine);
  		 }
  		 in.close();
  		        
  		 // print result
  		// LOGGER.info(response.toString());
  		 } else {
  		// LOGGER.info("POST request not worked");
  		 }}
  		 catch (Exception e)
  		 {
  		 //LOGGER.info("Error SMS "+e);
  		 }
  		 return responseCode;
  		 }
}
