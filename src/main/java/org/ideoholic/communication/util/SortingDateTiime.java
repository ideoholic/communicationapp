package org.ideoholic.communication.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.ideoholic.communication.dto.MessageDTO;

public class SortingDateTiime implements Comparator<MessageDTO>{

	@Override
	public int compare(MessageDTO o1, MessageDTO o2) {
		Date date1;
		Date date2;
		int DateCompare=0;
		try {
			date1=new SimpleDateFormat("dd/MM/yyyy").parse(o1.getDate());
			date2=new SimpleDateFormat("dd/MM/yyyy").parse(o2.getDate());
			DateCompare = date1.compareTo(date2);
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return DateCompare;
	}

}
