package org.ideoholic.communication.entity;


import java.sql.Time;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;
import org.ideoholic.communication.dto.UserDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


public class User  {
	
	public User() {
		super();
	}
	
	@Id
	private String id;
	
	@Field(value="isAdmin")
	private Boolean isAdmin=false;
	
	@Field(value="name")
	private String name;

	
	@NotBlank
	@Field(value="username")
	private String username; // username is same as mobile no
	
	@NotBlank
	@Field(value="password")
	private String password;
	
	@NotBlank
	@Field(value = "userid")
	private String userid;
	
	@Field(value="contact")
	private String contact;
	
	@NotBlank
	@Field(value="group")
	private String group;
	
	@Field(value="date")
	private String date;
	
	@Field(value="time")
	private String time;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getId() {
		return id;
	}
	 
	    public void setId(String id) {
			this.id= id;
		}
	    
	    public Boolean getIsAdmin() {
			return isAdmin;
		}
		public void setIsAdmin(Boolean isAdmin) {
			this.isAdmin = isAdmin;
		}
		
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
 

	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date= date;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
}