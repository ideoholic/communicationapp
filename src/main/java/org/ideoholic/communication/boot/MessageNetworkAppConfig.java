package org.ideoholic.communication.boot;

import org.ideoholic.communication.configuration.JwtAuthenticationEntryPoint;
import org.ideoholic.communication.configuration.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@ComponentScan("org.ideoholic")
@EntityScan
@EnableMongoRepositories(basePackages = "org.ideoholic")
@SpringBootApplication
public class MessageNetworkAppConfig {

    public MessageNetworkAppConfig() {
        super();
        // TODO Auto-generated constructor stub
    }

    public static void main(String args[]) {
        SpringApplication.run(MessageNetworkAppConfig.class, args);
    }

 
   
    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    	@Autowired
    	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    	
    	@Autowired
    	private UserDetailsService jwtUserDetailsService;

    	@Autowired
    	private JwtRequestFilter jwtRequestFilter;


    	@Autowired
    	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	// configure AuthenticationManager so that it knows from where to load
    	// user for matching credentials
    	// Use BCryptPasswordEncoder
    	auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
 
    	}
    	
    	@Bean
    	public PasswordEncoder passwordEncoder() {
    	return new BCryptPasswordEncoder();
    	}
    	
    	@Bean
    	@Override
    	public AuthenticationManager authenticationManagerBean() throws Exception {
    	return super.authenticationManagerBean();
    	}

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.
            authorizeRequests().
            antMatchers("/resources/**", "/rest/login").permitAll().
            antMatchers("/resources/**", "/rest/parentlogin").permitAll().
            antMatchers("/resources/**", "/rest/test").permitAll().
            antMatchers("/resources/**", "/rest/register").permitAll().
            anyRequest().authenticated()
            .and()
           .addFilterBefore(corsFilter(), ChannelProcessingFilter.class)
            .logout().permitAll().
            and()
            .csrf().disable();
            
            http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
        }

        @Bean
        public CorsFilter corsFilter() {
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            CorsConfiguration config = new CorsConfiguration();
            config.setAllowCredentials(true);
            config.addAllowedOrigin("*");
            config.addAllowedHeader("*");
            config.addAllowedMethod("OPTIONS");
            config.addAllowedMethod("GET");
            config.addAllowedMethod("POST");
            config.addAllowedMethod("PUT");
            config.addAllowedMethod("DELETE");
            config.addExposedHeader("Authorization");
            source.registerCorsConfiguration("/**", config);
            return new CorsFilter(source);
        }
    }
}
