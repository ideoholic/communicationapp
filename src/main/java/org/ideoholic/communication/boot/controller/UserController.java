package org.ideoholic.communication.boot.controller;



import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.ideoholic.communication.dto.UserDTO;
import org.ideoholic.communication.service.SecurityService;
import org.ideoholic.communication.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class UserController {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	private UserService userService;
	
    
    private SecurityService securityService;
  
    
    @Autowired
    private Environment env;
	
	@Autowired
	public UserController(UserService userService,SecurityService securityService) {
		this.securityService = securityService;
		this.userService = userService; 
		
	}
	
	//Get Authenticated Admin Details on logIn
	@RequestMapping(value = "/username", method = RequestMethod.GET)
	@ResponseBody
	  public UserDTO authenticatedUserDetails(Authentication authentication) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDTO user = userService.authenticatedUserDetails(auth);
	
		
		return user;		 
	}
	
	//Resgitration REST HANDLER
	@RequestMapping( value ="/register" ,method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	UserDTO create(@RequestBody @Valid UserDTO user) {
        
        UserDTO created = null;
   
        	created = userService.createUser(user);
      
        return created;
    }

  
	
	//DELETE REST HANDLER
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    UserDTO delete(@PathVariable("id") String id) {
        LOGGER.info("Deleting entry with id: {}", id);

        UserDTO deleted = userService.delete(id);
        LOGGER.info("Deleted need entry with information: {}", deleted);

        return deleted;
    }
    
    
    //GET USER DETAILS BASED ON PARTICULAR USER [PRIMARY-ID]
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    UserDTO userData(@PathVariable("id") String id) {
        LOGGER.info(" entry with id: {}", id);

        UserDTO userdetail = userService.userDetail(id);
        LOGGER.info("User entry with information: {}", userdetail);
        
        return userdetail;
    }
    
    //GET ALL USERS
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    List<UserDTO> getRegisteredUsers() {
       
        List<UserDTO> regiseteredUsers = null;
        try {
        	regiseteredUsers = userService.getRegisteredUser();
        	
        LOGGER.info("Registered users: {}", regiseteredUsers);
        }catch (Exception e) {    }
         return regiseteredUsers;
    }
    
    //Get Users based on search query (Groups)
    @RequestMapping(method = RequestMethod.GET)
    List<UserDTO> getSearchUsers(@RequestParam(value="group",defaultValue="1")String group){
    	
    	List<UserDTO> groupedUsers = userService.findSearchedUsers(group);
   
    	return groupedUsers;
    }
    
  
    //Update Details
    @RequestMapping(value="/update",method = RequestMethod.POST)
    UserDTO updateUserDetatils(@RequestBody @Valid UserDTO user) {	
    	UserDTO userInfo = userService.updateUsers(user);
		return userInfo;	
    }
    
    //Resend Sms
    @RequestMapping(value="/resend",method=RequestMethod.POST)
    String resendSms(@RequestBody @Valid UserDTO user) {
    	String response = userService.resend(user);
    	LOGGER.info("Message "+response);
		return response;	
    }
	
    
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
    
    

}

