package org.ideoholic.communication.boot.controller;

import java.util.List;

import javax.validation.Valid;

import org.ideoholic.communication.dto.GroupsDTO;
import org.ideoholic.communication.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("rest/group")
public class GroupController {
 
	private GroupService service;
	
	@Autowired
	public GroupController(GroupService groupService) {
		this.service = 	groupService;	
	}
	
	//Create a Group
	@RequestMapping(method=RequestMethod.POST)
	GroupsDTO createGroup(@RequestBody @Valid GroupsDTO groupCreate) {
		GroupsDTO groupDTO = service.create(groupCreate);
		return groupDTO;
	}
	
	//Delete a Group
	@RequestMapping(value = "{groupId}",method=RequestMethod.DELETE)
	GroupsDTO deleteGroup(@PathVariable("groupId") String groupId) {
		GroupsDTO group = service.delete(groupId);
		return group;
	}
	
	//Edit Group Name
	@RequestMapping(value="{groupId}",method=RequestMethod.PUT)
	GroupsDTO updateGroup(@PathVariable("groupId") String groupId,@RequestBody @Valid GroupsDTO groupUpdate) {
		GroupsDTO val =  service.update(groupUpdate,groupId);
		return val;
	}
	
	//Get All Group Names
	@RequestMapping(method=RequestMethod.GET)
	List<GroupsDTO> getGroups(){
		List<GroupsDTO> listOfGroups= service.getAll();
		return listOfGroups;
	}
}
