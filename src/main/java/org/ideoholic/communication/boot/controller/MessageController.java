package org.ideoholic.communication.boot.controller;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.ideoholic.communication.dto.MessageDTO;
import org.ideoholic.communication.dto.UserDTO;
import org.ideoholic.communication.service.MessageNotFoundException;
import org.ideoholic.communication.service.MessageService;
import org.ideoholic.communication.util.SortingDateTiime;

@RestController
@RequestMapping("/rest/{userId}/needs")
public class MessageController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);
	
	private final MessageService service;
	
	@Autowired
	public MessageController(MessageService needService) {
		this.service= needService; 
	}
	
	
	//CREATE A NEW MESSAGE HANDLER
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	MessageDTO create(@RequestBody @Valid MessageDTO todoEntry) {
  
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        LOGGER.info("Set the Authenticated user... : {}",name);
        
        MessageDTO created = service.create(todoEntry);
        LOGGER.info("Created a new need entry with information: {}", created.getTime());
		
        
        return created;
    }
	
	
	//DELETE A MESSAGE HANDLER
    @RequestMapping(value = "{messageId}", method = RequestMethod.DELETE)
    MessageDTO delete(@PathVariable("messageId") String messageid) {
        LOGGER.info("Deleting need entry with id: {}", messageid);

        MessageDTO deleted = service.delete(messageid);
        LOGGER.info("Deleted need entry with information: {}", deleted.getId());

        return deleted;
    }
    
   
    //RETRIEVING ALL MESSAONGES BASED ON USER ID
    @RequestMapping(method = RequestMethod.GET)
    List<MessageDTO> findAll(@PathVariable("userId") String Userid) {
        LOGGER.info("Finding all need entries",Userid);

        List<MessageDTO> todoEntries = service.findBy(Userid);
        LOGGER.info("Found {} need entries", todoEntries.size());

        return todoEntries;
    }
    
    //RETRIEVING A PARTICULAR MESSAGE BASED ON MESSAGE ID AND USER ID
    @RequestMapping(value = "{messageId}",method = RequestMethod.GET)
    List<MessageDTO> findAParticular(@PathVariable("userId") String Userid,@PathVariable("messageId") String messageid) {
        LOGGER.info("Finding all need entries",Userid);

        List<MessageDTO> todoEntries = service.findByMessageId(Userid,messageid);
        LOGGER.info("Found {} need entries", todoEntries.size());

        return todoEntries;
    }
   

   //Send Message to the Some users
	@RequestMapping(value = "messageId",method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	MessageDTO sendMessage(@PathVariable("userId") String Userid,@RequestBody @Valid MessageDTO todoEntry) {
          
		todoEntry.setUserId(Userid);
        MessageDTO created = service.sendTo(todoEntry); 
        created.setUserId(Userid);
        
        return created;
    }
	
	//Get Messages to a Parent belonging to a Particular Class
	@RequestMapping(value = "parent/{parentID}",method=RequestMethod.GET)
	 List<MessageDTO> getParentMessage(@PathVariable("parentID") String parentId) {
		
		 List<MessageDTO> list1 = service.findMessagesBygroup(parentId);
		 List<MessageDTO> list2 = service. findMessagesByParentId(parentId); 
		 List<MessageDTO> list =  new ArrayList<MessageDTO>(list1);
	        
		 list.addAll(list2);
		 
		 Collections.sort(list, new SortingDateTiime()); 
	     Collections.reverse(list); 
		 
		return list;	
	}
	
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleTodoNotFound(MessageNotFoundException ex) {
        LOGGER.error("Handling error with message: {}", ex.getMessage());
    }

}
