package org.ideoholic.communication.service;

import static java.util.stream.Collectors.toList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.ideoholic.communication.data.repository.MessageRepository;
import org.ideoholic.communication.data.repository.UserRepository;
import org.ideoholic.communication.dto.MessageDTO;
import org.ideoholic.communication.entity.Message;
import org.ideoholic.communication.entity.User;
import org.ideoholic.communication.util.SortingDateTiime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);

	private final MessageRepository repository;
	private final UserRepository userRepo;

	@Autowired
	MessageServiceImpl(MessageRepository repository, UserRepository userRepo) {
		this.repository = repository;
		this.userRepo = userRepo;
	}

	// MESSAGE CREATION METHOD
	public MessageDTO create(MessageDTO need) {
		// TODO Auto-generated method stub

		Message persistedMessage = new Message();

		SimpleDateFormat objTime = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat objDate = new SimpleDateFormat("dd/MM/yyyy");

		String time = objTime.format(Calendar.getInstance().getTime());
		String date = objDate.format(Calendar.getInstance().getTime());

		String userid = "1";

		need.setTime(time);
		need.setDate(date);

		need.setUserId(userid);

		persistedMessage.setUserId(need.getUserId());
		persistedMessage.setSubject(need.getSubject());
		persistedMessage.setDescription(need.getDescription());
		persistedMessage.setGroup(need.getGroup());
		persistedMessage.setTime(need.getTime());
		persistedMessage.setDate(need.getDate());
		persistedMessage.setTimepicker(need.getTimepicker());
		persistedMessage.setDatepicker(need.getDatepicker());
		
		persistedMessage = repository.save(persistedMessage);
		
		return convertToMessageDTO(persistedMessage);
	}
	
	//Send messages to some Users
	public MessageDTO sendTo(MessageDTO need) {
		// TODO Auto-generated method stub

		Message persistedMessage = new Message();

		SimpleDateFormat objTime = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat objDate = new SimpleDateFormat("dd/MM/yyyy");

		String time = objTime.format(Calendar.getInstance().getTime());
		String date = objDate.format(Calendar.getInstance().getTime());

		need.setTime(time);
		need.setDate(date);

		

		persistedMessage.setUserId(need.getUserId());
		persistedMessage.setSubject(need.getSubject());
		persistedMessage.setDescription(need.getDescription());
		persistedMessage.setGroup(need.getGroup());
		persistedMessage.setTime(need.getTime());
		persistedMessage.setDate(need.getDate());
		persistedMessage.setUsers(need.getUsers());

		persistedMessage = repository.save(persistedMessage);
		
		return convertToMessageDTO(persistedMessage);
	}
	
	// MESSAGE DELETION METHOD
	public MessageDTO delete(String id) {

		Message need = findMessageById(id);
		repository.delete(need);
		return convertToMessageDTO(need);

	}
	
     private Message findMessageById(String id) {
    	 Message result =
	 repository.findOne(id); return result;
	 }
	
     /* 
	 * public List<MessageDTO> findAll() { List<Message> needEntries =
	 * repository.findAll(); return convertToDTOs(needEntries); }
	 */

	// RETRIEVING ALL MESSAGES
	public List<MessageDTO> findBy(String UserId) {
		List<Message> needEntries = repository.findByUserId(UserId);
		List<MessageDTO> listOfMessages = convertToDTOs(needEntries);
		 
        Collections.sort(listOfMessages, new SortingDateTiime()); 
        Collections.reverse(listOfMessages); 
		return listOfMessages;
	}


	// RETRIEVING A PARTICULAR MESSAGE
	public List<MessageDTO> findByMessageId(String UserId, String MessageId) {
		List<Message> needEntries = repository.findByUserMessageId(UserId, MessageId);
		List<MessageDTO> listOfMessages = convertToDTOs(needEntries);
			   
		return listOfMessages;
	}

	private List<MessageDTO> convertToDTOs(List<Message> needEntries) {
		return needEntries.stream().map(this::convertToMessageDTO).collect(toList());};


	
	private MessageDTO convertToMessageDTO(Message need) {

		MessageDTO needDTO = new MessageDTO();
		needDTO.setId(need.getId());
		needDTO.setUserId(need.getUserId());
		needDTO.setTime(need.getTime());
		needDTO.setDate(need.getDate());
		needDTO.setDescription(need.getDescription());
		needDTO.setSubject(need.getSubject());
		needDTO.setGroup(need.getGroup());
		needDTO.setUsers(need.getUsers());
		needDTO.setDatepicker(need.getDatepicker());
		needDTO.setTimepicker(need.getTimepicker());
		
		return needDTO;
	}

	
	 @Override public List<MessageDTO> findCreatedBy(String userName) {
	 List<Message> needEntries = new ArrayList<Message>(); return
	 convertToDTOs(needEntries); }

	 
	//Get the lists of messages of a parent belonging to a particular class/group 
	@Override
	public  List<MessageDTO> findMessagesBygroup(String parentId) {
		String group = groups(parentId);	
		List<Message> needEntries = repository.findByMessageGroup(group);
		
		return convertToDTOs(needEntries);
	}
	
	//Get a group
	String groups(String parentId) {	
		User user = userRepo.findById(parentId);	
		String groupName = user.getGroup();
		return groupName;
	}
	
	//Get the lists of messages of a parent based on parent id
	@Override
	public  List<MessageDTO> findMessagesByParentId(String parentId) {
		List<Message> needEntries = repository.findByParentId(parentId);
		
		return convertToDTOs(needEntries);
	}
	

}
