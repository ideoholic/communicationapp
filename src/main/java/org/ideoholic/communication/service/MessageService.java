package org.ideoholic.communication.service;

import java.util.List;


import org.ideoholic.communication.dto.MessageDTO;

 public interface  MessageService {
	   
	 	MessageDTO create(MessageDTO need);
		
		MessageDTO delete(String id);
			
		List<MessageDTO> findBy(String UserId);
		
	    List<MessageDTO> findByMessageId(String UserId,String Message );
		
		List<MessageDTO> findCreatedBy(String userName);
 
		MessageDTO sendTo(MessageDTO todoEntry);
		 
		 List<MessageDTO> findMessagesBygroup(String parentId);
		
		 List<MessageDTO> findMessagesByParentId(String parentId);
		
}
