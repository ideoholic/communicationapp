package org.ideoholic.communication.service;


public class MessageNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageNotFoundException(String id) {
		
		super(String.format("No Message entry found with id: <%s>", id));
	}

}
