package org.ideoholic.communication.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.ideoholic.communication.boot.controller.UserController;
import org.ideoholic.communication.data.repository.UserRepository;
import org.ideoholic.communication.entity.User;

public class UserDetailServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails loadedUser;
		
		LOGGER.info("Inside UserDetails Service::",username);
		try {
		   
			User user = userRepository.findByusername(username);
			LOGGER.info("User  found");
			
		
		if(user == null) {
			LOGGER.info("User is not found");
			
			throw new UsernameNotFoundException("could not find the user:"+username);
		}
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(user.getPassword());
		loadedUser = new org.springframework.security.core.userdetails.User(user.getUsername(),hashedPassword,true, true, true, true, AuthorityUtils.createAuthorityList("USER"));

		
		
		}catch(Exception ex) {
			throw new InternalAuthenticationServiceException(ex.getMessage(),ex);
		}		
		return loadedUser;
	}

}
