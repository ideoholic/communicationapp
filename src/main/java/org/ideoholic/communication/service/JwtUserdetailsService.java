package org.ideoholic.communication.service;

import java.util.ArrayList;

import org.ideoholic.communication.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class  JwtUserdetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
	
		
		org.ideoholic.communication.entity.User checkUser = userRepository.findByusername(username);
		
		String name = checkUser.getUsername();
		String password = checkUser.getPassword();
		
		//String password = "123456";
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		
		if(name.equals(username)) {
			return new User(name,hashedPassword,
					new ArrayList<>());
			
		}else {
			throw new UsernameNotFoundException("User not found Exception");
		}


	}
	

	public UserDetails loadUserByAdmin(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		org.ideoholic.communication.entity.User checkUser = userRepository.findByusername(username);
		
		String name = checkUser.getUsername();
		String password = checkUser.getPassword();
		
		//String password = "123456";
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		
		
		if(name.equals(username) && checkUser.getIsAdmin() == true ) {
			return new User(name,hashedPassword,
					new ArrayList<>());
			
		}else {
			throw new UsernameNotFoundException("User not found Exception");
		}

	}

	public UserDetails loadUserByParent(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
	
		
		org.ideoholic.communication.entity.User checkUser = userRepository.findByusername(username);
		
		String name = checkUser.getUsername();
		String password = checkUser.getPassword();
		
		//String password = "123456";
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		
		
		if(name.equals(username) && checkUser.getIsAdmin() == false ) {
			return new User(name,hashedPassword,
					new ArrayList<>());
			
		}else {
			throw new UsernameNotFoundException("User not found Exception");
		}

	}

}
