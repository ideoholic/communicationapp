package org.ideoholic.communication.service;

import java.util.List;

import org.ideoholic.communication.dto.GroupsDTO;


public interface GroupService {
    GroupsDTO create(GroupsDTO groupCreate);
    GroupsDTO delete(String groupDelete);    
    GroupsDTO update(GroupsDTO groupUpdate,String id);
    List<GroupsDTO> getAll();
}
