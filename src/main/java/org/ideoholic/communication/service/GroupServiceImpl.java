package org.ideoholic.communication.service;

import org.ideoholic.communication.data.repository.GroupsRepository;
import org.ideoholic.communication.dto.GroupsDTO;
import org.ideoholic.communication.dto.MessageDTO;
import org.ideoholic.communication.entity.Groups;
import org.ideoholic.communication.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService{
	
	private GroupsRepository repository;
	
	@Autowired
	public GroupServiceImpl(GroupsRepository repo) {
		this.repository = repo;
	}
	//Creation of a new Group
	 public GroupsDTO create(GroupsDTO groupCreate) {
		 
		Groups groupObj = new Groups();
		groupObj.setGroupName(groupCreate.getGroupName());
		groupObj = repository.save(groupObj);
		GroupsDTO groupDTO = convertToGroupDTO(groupObj);
		return groupDTO;	 
	 }

	private GroupsDTO convertToGroupDTO(Groups groupObj) {
		// TODO Auto-generated method stub
		GroupsDTO groupDTO = new GroupsDTO();
		groupDTO.setGroupName(groupObj.getGroupName());
		groupDTO.setId((groupObj.getId()));
		return groupDTO;
	}
	
	 //Deletion of a Group
	public GroupsDTO delete(String groupDeleteId) {
		Groups group = findGroupById(groupDeleteId);

		repository.delete(group);
		return convertToGroupDTO(group);
	
	}
	
	//Will find the Group By id and Return the Group
	private Groups findGroupById(String groupDeleteId) {
		// TODO Auto-generated method stub
		 Groups result =repository.findOne(groupDeleteId);
		 return result;

	}
	
	
	//Update Group
	public GroupsDTO update(GroupsDTO groupUpdate,String id) {
		// TODO Auto-generated method stub
		Groups groupObj = findGroupById(id);
		
		groupObj.setGroupName(groupUpdate.getGroupName());
		repository.save(groupObj);
		
		return convertToGroupDTO(groupObj);
	}
	
	 //Get All Groups
	public List<GroupsDTO> getAll() {
		List<Groups> groups = repository.findAll();
		return convertToDTOs(groups);
	}
	
	private List<GroupsDTO> convertToDTOs(List<Groups> groups) {
		return groups.stream()
				.map(this::convertToGroupDTO).
				collect(toList());
		}; 
	
}
