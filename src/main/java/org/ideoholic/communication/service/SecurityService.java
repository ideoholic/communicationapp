package org.ideoholic.communication.service;

public interface SecurityService {
	

	    String findLoggedInUsername();

	    void login(String username, String password);
	}


