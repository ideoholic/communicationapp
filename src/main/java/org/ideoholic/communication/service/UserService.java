package org.ideoholic.communication.service;

import java.util.List;

import org.ideoholic.communication.dto.MessageDTO;
import org.ideoholic.communication.dto.UserDTO;
import org.ideoholic.communication.entity.User;
import org.springframework.security.core.Authentication;

public interface UserService {
	
	UserDTO createUser(UserDTO user);
	
	UserDTO delete(String id);
	
	List<UserDTO> findByUser(String userid);
	UserDTO findByUserName(String username);

    UserDTO updateUser(String id);
	UserDTO update(UserDTO user);
	
	UserDTO updateUserId(String username,String userid);
	
	List<UserDTO> getRegisteredUser();

	void createPasswordResetTokenForUser(UserDTO user, String token);

	List<MessageDTO> findMessagesForUser(String string);

	UserDTO updateContact(String username, String contact);

	UserDTO userDetail(String id);

	List<UserDTO> findSearchedUsers(String group);
	
	UserDTO authenticatedUserDetails(Authentication authentication);
	
	UserDTO updateUsers(UserDTO user);
	
	String resend(UserDTO user);
}
