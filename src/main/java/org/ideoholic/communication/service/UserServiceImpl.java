package org.ideoholic.communication.service;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.ideoholic.communication.data.repository.MessageRepository;

import org.ideoholic.communication.data.repository.UserRepository;

import org.ideoholic.communication.dto.MessageDTO;
import org.ideoholic.communication.dto.UserDTO;
import org.ideoholic.communication.entity.Message;

import org.ideoholic.communication.entity.User;
import org.ideoholic.communication.exception.ServiceException;
import org.ideoholic.communication.util.SendSms;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	private final UserRepository repository;
	private final MessageRepository needRepository;
	SendSms sms = SendSms.getInstance(); 
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private User user;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, MessageRepository needRepo) {
		this.needRepository = needRepo;
		this.repository = userRepository;
	}

	public UserDTO createUser(UserDTO user) {

		User checkUser = repository.findByusername(user.getUsername());
		LOGGER.info("Check USer "+"  USwerDtO "+ user.getUsername());
		if (checkUser == null) {
			User savedUser = new User();
			
			SimpleDateFormat ObjTime = new SimpleDateFormat("HH:mm:ss");
			SimpleDateFormat ObjDate = new SimpleDateFormat("dd/MM/yyyy");


			String time = ObjTime.format(Calendar.getInstance().getTime());
			String date = ObjDate.format(Calendar.getInstance().getTime());


			user.setTime(time);
			user.setDate(date);


			savedUser.setName(user.getName());
			savedUser.setUsername(user.getUsername());
			savedUser.setPassword(user.getPassword().toString());
			savedUser.setUserid(user.getUserid());
			savedUser.setTime(user.getTime());
			savedUser.setDate(user.getDate());

			savedUser.setContact(user.getContact());
			savedUser.setGroup(user.getGroup());
			savedUser.setIsAdmin(user.getIsAdmin());

			savedUser = repository.save(savedUser);
			
			String numbers = savedUser.getContact();
			
			//Send an SmS
			String message = "Hii "+savedUser.getName()+" your username and password is "+
			savedUser.getUsername()+"   "+savedUser.getPassword();
			int responseCode = sms.sendSmsFunc(numbers, message);
			
			return convertToDTO(savedUser);
		
		 }
		else
		{
			LOGGER.info("User Already exists . Please invoke login."); 
			throw new ServiceException("User Already Exists");
		 }
		 

	}
	
	
	
	//Delete users
	public UserDTO delete(String id) {
		User deleted = repository.findOne(id);
		repository.delete(deleted);
		return convertToDTO(deleted);
	}

	 public List<MessageDTO> findMessagesForUser(String username) { 
		 List<Message>  needEntries = needRepository.findBycreatedBy(username); 
		 
		 return convertToDTOs(needEntries); }

	private List<MessageDTO> convertToDTOs(List<Message> needEntries) {
		return needEntries.stream().map(this::convertToMessageDTO).collect(toList());
	}
	
	public List<UserDTO> findByUser(String userid) {
		LOGGER.info("Check the user...." + userid);
		List<User> userFound = repository.findByUserId(userid);
		LOGGER.info("Got the User" + userFound);
		return convertToDTOonRetrieval(userFound);
	}
	
	private List<UserDTO> convertToDTOonRetrieval(List<User> needEntries) {
		return needEntries.stream().map(this::convertToDTO).collect(toList());
	}
	
	private UserDTO convertToDTO(User user) {

		if (user != null) {
			LOGGER.info("user id 2222"+user.getUserid());
			UserDTO userDTO = new UserDTO();
			userDTO.setId(user.getId().toString());
			userDTO.setName(user.getName());
			userDTO.setPassword(user.getPassword().toString());
			userDTO.setUsername(user.getUsername());
			userDTO.setUserid(user.getUserid());
			userDTO.setContact(user.getContact());
			userDTO.setDate(user.getDate());
			userDTO.setTime(user.getTime());
			userDTO.setGroup(user.getGroup());
			userDTO.setIsAdmin(user.getIsAdmin());
			
			LOGGER.info("user id 3333"+userDTO.getUserid());

			return userDTO;
		} else {
			return null;
		}

	}

	private User convertToUserRepo(UserDTO user) {

		User savedUser = new User();
		if (user != null) {
			savedUser.setUsername(user.getUsername());
			savedUser.setName(user.getName());
			savedUser.setPassword(user.getPassword().toString());
			savedUser.setUserid(user.getUserid());
			savedUser.setContact(user.getContact());
			savedUser.setGroup(user.getGroup());
			savedUser.setDate(user.getDate());
			savedUser.setTime(user.getTime());

		}
		return savedUser;
	}

	private MessageDTO convertToMessageDTO(Message need) {

		MessageDTO needDTO = new MessageDTO();
		
		needDTO.setId(need.getId());
		needDTO.setCreatedBy(need.getCreatedBy());
		needDTO.setDescription(need.getDescription());
		
		return needDTO;
	}

	@Override
	public void createPasswordResetTokenForUser(UserDTO user, String token) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UserDTO> getRegisteredUser() {
		List<User> registeredUser = repository.findAll();
		List<UserDTO> userslist = new ArrayList<UserDTO>();
			
		for (User user : registeredUser) {	
				UserDTO users = convertToDTO(user);
				LOGGER.info("I ma here");
			   if(!users.getIsAdmin())
				userslist.add(users);
			}
		return userslist;

	}
	
	public UserDTO findByUserName(String username) {
		User userFound = repository.findByusername(username);
		return convertToDTO(userFound);
	}

	@Override
	public UserDTO updateContact(String username, String contact) {
		
		return null;
	}
	
	@Override
	public UserDTO updateUser(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDTO updateUserId(String username, String userid) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setUser(User user) {
        this.user = user;
    }

	@Override
	public UserDTO update(UserDTO user) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public UserDTO userDetail(String id) {
		User userdetails = repository.findOne(id); 
		return convertToDTO(userdetails);
	}

	//Will retrieve searched users based on group
	@Override
	public List<UserDTO> findSearchedUsers(String group) {
		// TODO Auto-generated method stub
		List<User> User = repository.findByGroup(group);
		
		List<UserDTO> userslist = new ArrayList<UserDTO>();
			
		for (User user : User) {	
				UserDTO users = convertToDTO(user);
				userslist.add(users);
			}
		return userslist;
		
	}

	
	//Get Details of an Authenticated Admin
	@Override
	public UserDTO authenticatedUserDetails(Authentication authentication) {
		// TODO Auto-generated method stub
		User user = repository.findByusername(authentication.getName());
		LOGGER.info("user id 111"+user.getUserid());
		return convertToDTO(user);
	}

	//Update USer information
	@Override
	public UserDTO updateUsers(UserDTO user) {
		String id = user.getId();
		String name = user.getName();
		
		User users = repository.findById(id);
		
		users.setName(user.getName());
		users.setUsername(user.getUsername());
		users.setPassword(user.getPassword());
		users.setContact(user.getContact());
		users.setUserid(user.getUserid());
		users.setGroup(user.getGroup());
		
		LOGGER.info("Updated Values "+user.getName()+"  "+users.getUserid());
		User userss = repository.save(users);
		
		return convertToDTO(userss);
	}

	//Resecnd SMS
	@Override
	public String resend(UserDTO user) {
		
		String numbers = user.getContact();
		String message = "Hii "+user.getName()+" your username and password is "+
				user.getUsername()+"   "+user.getPassword();
		
		int responseCode = sms.sendSmsFunc(numbers, message);
		return "Sent successfully";
	}
	

}
