package org.ideoholic.communication.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import org.ideoholic.communication.dto.UserDTO;


@Service
public class MessageAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	UserService userService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageAuthenticationProvider.class);
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected UserDetails retrieveUser(String username,UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		
		UserDetails loadedUser;
		UserDTO user ;
		/* try {
			 LOGGER.info("Invoking user service {}.."+username);
			 
			 user = userService.findByUserName(username);
			 
	         loadedUser = new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
	    		  } 
		 catch (Exception repositoryProblem) {
	            throw new InternalAuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
	    		  }*/
		user = userService.findByUserName(username);
		String Username = user.getUsername();
		String Userpassword = user.getPassword();
		
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(Userpassword);
		
		LOGGER.info("Invoking user service {}.."+username);
		
		if(Username.equals(username)) {
			LOGGER.info("No error dude"+user.getUsername()+"-------"+user.getPassword());
			return new User(Username,hashedPassword,
					new ArrayList<>());
			
		}else {
			throw new UsernameNotFoundException("User not found Exception");
		}

	}
	
	
}

