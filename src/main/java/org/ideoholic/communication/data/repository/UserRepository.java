package org.ideoholic.communication.data.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.ideoholic.communication.entity.Message;
import org.ideoholic.communication.entity.User;

@RepositoryRestResource(collectionResourceRel = "need", path = "need")


public interface UserRepository  extends MongoRepository<User, String>{
	
	void delete(User user);
	List<User> findAll(); 
	User findByusername(String username);
	
	@SuppressWarnings("unchecked")
	User save(User saved);
	
	@Query (value = "{'userid':?0}") 
	List<User> findByUserId(String id);
	
	@Query (value = "{'group':?0}") 
	List<User> findByGroup(String group);
	
	@Query (value = "{'id':?0}") 
	User findById(String id);
	
  
   

}
