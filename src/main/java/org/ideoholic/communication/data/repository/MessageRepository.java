package org.ideoholic.communication.data.repository;

import java.util.List;

import org.ideoholic.communication.entity.Message;
import org.ideoholic.communication.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

//*@RepositoryRestResource(collectionResourceRel = "need", path = "need")*/
public interface MessageRepository extends MongoRepository<Message, String> {

	void delete(Message need);
  
	List<Message> findAll();

	Message findOne(@Param("id") String id);

	// List<Message> findBycreatedBy(String createdBy);

	@SuppressWarnings("unchecked")
	Message save(Message saved);

	
	  @Query(value = "{'users': ?0} ") 
	  List<Message> findMessageByUsers(String[]
	  username);
	   
	  @Query(value = "{'userid': ?0}") List<Message> findByUserId(String
	  userid);
	  
	  @Query(value = "{'id': ?0}") 
	  List<Message> findByMessageId(String id);
	  
	  @Query(value = "{'userid': ?0,'id': ?1}") List<Message> findByUserMessageId(String
			  userid,String id);

	 List<Message> findBycreatedBy(String username);
	 
	 @Query(value = "{'group': ?0}") 
	  List<Message> findByMessageGroup(String group);
	 
	 @Query(value = "{'users': ?0}") 
	  List<Message> findByParentId(String parentId);
	  
	  /*
	  List<Message> findByUsersIn(List<String> username);
	  
	  @Query(value = "{'from': {$regex:?0}}") List<Message> findByUsers(String
	  username);
	 

}*/
}
