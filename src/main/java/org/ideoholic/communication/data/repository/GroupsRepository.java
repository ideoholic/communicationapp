package org.ideoholic.communication.data.repository;

import java.util.List;

import org.ideoholic.communication.entity.Groups;
import org.ideoholic.communication.entity.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

public interface GroupsRepository extends MongoRepository<Groups, String>{

	//Will save the group
	@SuppressWarnings("unchecked")
	Groups save(Groups saved);
	
	//Will try to find the particular group by id and will return respective one
	Groups findOne(@Param("id") String id);
	
	//Will delete the group
	void delete(Groups group);
	
	//Get all groups
	List<Groups> findAll();
	
}
