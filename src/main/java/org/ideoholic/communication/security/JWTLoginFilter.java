package org.ideoholic.communication.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.ideoholic.communication.data.repository.MessageRepository;
import org.ideoholic.communication.dto.UserDTO;
import org.ideoholic.communication.service.MessageAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageAuthenticationProvider.class);
	
	private final AuthenticationSuccessHandler successHandler;
    private final AuthenticationFailureHandler failureHandler;

	public JWTLoginFilter(String url, AuthenticationManager authManager,AuthenticationSuccessHandler successHandler,AuthenticationFailureHandler failureHandler) {
	    super(new AntPathRequestMatcher(url));
	    LOGGER.info("Invoking Login Filter JWTLoginFilter {}..");
	    setAuthenticationManager(authManager);
	    this.successHandler =successHandler;
	    this.failureHandler = failureHandler;
    
  }
  @Override
  public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
      throws AuthenticationException, IOException, ServletException {
    
	  UserDTO creds = new ObjectMapper()
        .readValue(req.getInputStream(), UserDTO.class);
    
	  LOGGER.info("Invoking Login Filter attemptAuthentication {}..");
   
    return getAuthenticationManager().authenticate(
        new UsernamePasswordAuthenticationToken(
            creds.getUsername().toString(),
            creds.getPassword().toString(),
            Collections.emptyList()
        )
    );
  }
  @Override
  protected void successfulAuthentication(
      HttpServletRequest req,
      HttpServletResponse res, FilterChain chain,
      Authentication auth) throws IOException, ServletException {
   
	 // TokenAuthenticationService
     //   .addAuthentication(res, auth.getName());
	  LOGGER.info("Invoking Login Filter successfulAuthentication {}..");
	  successHandler.onAuthenticationSuccess(req, res, auth);
	  
  }
  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
          AuthenticationException failed) throws IOException, ServletException {
	  LOGGER.info("Invoking Login Filter unsuccessfulAuthentication {}..");
      failureHandler.onAuthenticationFailure(request, response, failed);
  }
}
